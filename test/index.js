'use strict';

var Lab = require('lab');
var lab = exports.lab = Lab.script();
var describe = lab.describe;
var it = lab.it;
var before = lab.before;
var expect = require('code').expect;

var Hapi = require('hapi');

describe('hapi-trailingslash', function () {

    describe('options', function() {
        it('should not error when no options are passed', function (done) {

            var server = new Hapi.Server();
            server.connection({ port: 3000});
            server.register(require('../'), function (err) {
                expect(err).to.not.exist();
                done();
            });
        });

        it('should return error when invalid options are passed', function (done) {

            var server = new Hapi.Server();
            server.connection({ port: 3000});

            server.register({ register: require('../'), options: { behavior: 5 }}, function (err) {
                expect(err).to.exist();
                expect(err).to.be.an.instanceOf(Error);
                expect(err.name).to.equal('ValidationError');
                done();
            });
        });

        it('should return error when hapi stripTrailingSlash===true', function (done) {

            var server = new Hapi.Server({ connections: { router: { stripTrailingSlash:true }}});
            server.connection({ port: 3000 });

            server.register(require('../'), function (err) {
                expect(err).to.exist();
                expect(err).to.be.an.instanceOf(Error);
                done();
            });
        });

        it('should allow "strip" as a behaviour', function (done) {

            var server = new Hapi.Server();
            server.connection({ port: 3000 });

            server.register({ register: require('../'), options: { behavior: 'strip' }}, function (err) {
                expect(err).to.not.exist();
                done();
            });
        });

        it('should allow "append" as a behaviour', function (done) {

            var server = new Hapi.Server();
            server.connection({ port: 3000 });

            server.register({ register: require('../'), options: { behavior: 'append' }}, function (err) {
                expect(err).to.not.exist();
                done();
            });
        });
    });

    describe('strip', function () {

        var server;
        before(function(done) {

            server = new Hapi.Server();
            server.connection({ port: 3000});

            server.register({ register: require('../'), options: { behavior: 'strip' }}, function (err) {

                server.route({
                    method: 'GET',
                    path: '/',
                    handler: function (request, reply) {
                        reply('Hi from the root!');
                    }
                });

                done();
            });
        });

        it('should not do anything for requests to root', function (done) {

            server.inject({
                url: '/',
            }, function (response) {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.equal('Hi from the root!');
                done();
            });
        });

        it('should strip a tailing slash', function (done) {

            server.route({
                method: 'GET',
                path: '/path/',
                handler: function (request, reply) {
                    reply('Hi from the path!');
                }
            });

            server.inject({
                url: '/path/',
            }, function (response) {
                expect(response.statusCode).to.equal(301);
                expect(response.headers.location).to.equal('/path');
                done();
            });
        });

        it('should not do anything if there is no trailing slash', function (done) {

            server.route({
                method: 'GET',
                path: '/path',
                handler: function (request, reply) {
                    reply('Hi from the path!');
                }
            });

            server.inject({
                url: '/path',
            }, function (response) {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.equal('Hi from the path!');
                done();
            });
        });
    });

    describe('append', function () {

        var server;
        before(function(done) {

            server = new Hapi.Server();
            server.connection({ port: 3000});

            server.register({ register: require('../'), options: { behavior: 'append' }}, function (err) {

                server.route({
                    method: 'GET',
                    path: '/',
                    handler: function (request, reply) {
                        reply('Hi from the root!');
                    }
                });

                done();
            });
        });

        it('should not do anything for requests to root', function (done) {

            server.inject({
                url: '/',
            }, function (response) {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.equal('Hi from the root!');
                done();
            });
        });

        it('should append a tailing slash', function (done) {

            server.route({
                method: 'GET',
                path: '/path',
                handler: function (request, reply) {
                    reply('Hi from the path!');
                }
            });

            server.inject({
                url: '/path',
            }, function (response) {
                expect(response.statusCode).to.equal(301);
                expect(response.headers.location).to.equal('/path/');
                done();
            });
        });

        it('should not do anything if there is already a trailing slash', function (done) {

            server.route({
                method: 'GET',
                path: '/path/',
                handler: function (request, reply) {
                    reply('Hi from the path!');
                }
            });

            server.inject({
                url: '/path/',
            }, function (response) {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.equal('Hi from the path!');
                done();
            });
        });
    });
});
