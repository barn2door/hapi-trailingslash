'use strict';

var Joi = require('joi');
var Hoek = require('hoek');

var internals = {
    STRIP: 'strip',
    APPEND: 'append'
};

internals.schema = Joi.object().keys({
    behavior: Joi.string().valid([internals.STRIP, internals.APPEND]).optional()
});

internals.defaults = {
    behavior: undefined
};

exports.register = function (server, options, next) {

    if(server.settings.connections.router.stripTrailingSlash) {
        var name = exports.register.attributes.pkg.name;
        return next(new Error( name + ' plugin not compatible with hapi when router.stripTrailingSlash===true'));
    }

    // Ensure options passed in are valid and merge with defaults
    var validateOptions = internals.schema.validate(options);
    if (validateOptions.error) {
        return next(validateOptions.error);
    }

    var settings = Hoek.applyToDefaults(internals.defaults, options);

    if(settings.behavior) {

        // If strip === true, remove the slash, otherwise behaviour is
        // to append
        var strip = settings.behavior === internals.STRIP;
        var append = !strip;

        server.ext('onRequest', function (request, reply) {

            if(request.path.length > 1) {

                var urlEndsWithSlash = request.url.pathname[request.url.pathname.length - 1] === '/';

                if(strip && urlEndsWithSlash) {
                    request.url.pathname = request.url.pathname.slice(0, -1);
                    return reply('Redirecting to url without trailing slash')
                        .redirect(request.url.format())
                        .permanent();
                }

                if(append && !urlEndsWithSlash) {
                    request.url.pathname = request.url.pathname + '/';
                    return reply('Redirecting to url with trailing slash')
                        .redirect(request.url.format())
                        .permanent();
                }
            }

            reply.continue();
        });
    }

    next();
};

exports.register.attributes = {
    pkg: require('../package.json')
};
